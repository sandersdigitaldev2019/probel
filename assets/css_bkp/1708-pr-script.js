$(document).ready(function () {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const cep = urlParams.get('slwaddress');

    setTimeout(() => {
        $('#storelocator-search_address').val(cep);
    }, 5000);


    function changeWhatsapp() {
        $(".storelocator-fax a").each(function () {
            var numero = $(this).text();
            var num = numero.replace(/[^0-9\.]/g, '');
            $(this).addClass('whatsapp');
            $(this).attr('target', '_blank');
            $(this).attr('href', 'https://wa.me/55' + num);
        });
    }
    var interval = setInterval(function () {
        changeWhatsapp()
    }, 500);

    setTimeout(() => {
        clearInterval(interval);
    }, 3000);

    $('input#storelocator-search_address').on('keyup change', function () {
        var interval = setInterval(function () {
            changeWhatsapp()
        }, 500);

        setTimeout(() => {
            clearInterval(interval);
        }, 3000);
    });
    $(window).change(function (e) {
        var interval = setInterval(function () {
            changeWhatsapp()
        }, 500);

        setTimeout(() => {
            clearInterval(interval);
        }, 3000);
    });
    $('ul.ui-widget-content.ui-autocomplete').click(function (e) {
        var interval = setInterval(function () {
            changeWhatsapp()
        }, 500);

        setTimeout(() => {
            clearInterval(interval);
        }, 3000);
    });

    let screen = $('body').width();

    let codeEplusAlpha = {

        accountHeader: function () {
            $('.topo .account-topo p').click(function () {
                $('.topo .account-topo .account-itens').slideToggle()
            })
        },

        campoBusca: function () {
            $('.topo .busca-topo input').focusin(function () {
                $('.topo .busca-topo').addClass('ativo')
            })

            $('.topo .busca-topo .prompt').focusout(function () {
                $('.topo .busca-topo').removeClass('ativo')
            })

            $('.topo .busca-topo img').click(function (e) {
                if ($('.topo .busca-topo .prompt').val().length > 0) {
                    $('.topo .busca-topo .prompt').trigger($.Event("keypress", {
                        which: 13
                    }))
                } else {
                    alert('digite um termo para ser buscado')
                }
            })
        },

        slidePrincipal: function () {
            setTimeout(function () {
                $('.slide-principal').slick({
                    infinite: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: true,
                    autoplay: true,
                    autoplaySpeed: 8000
                })
            })
        },

        slideAvisos: function () {
            if (screen < 999) {
                $('.avisos .row').slick({
                    arrows: false,
                    slidesToShow: 1,
                    slidesToScroll: 1
                });
            }
        },

        slidePrateleira: function () {
            $('.box-vitrine .vitrine-prod').slick({
                infinite: true,
                slidesToShow: 4,
                slidesToScroll: 4,
                dots: false,
                responsive: [{
                    breakpoint: 999,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        infinite: false
                    }
                }]
            })
        },

        bannersHome: function () {
            let bannerCentral = $('.banner-central').html()
            $('.colecao-2134').before('<div class="container banner-central">' + bannerCentral + '</div>')
            $('.banner-central').last().remove()
        },

        instaFeed: function (username) {
            $.instagramFeed({
                'username': username,
                'container': '#instagram',
                'display_profile': false,
                'display_biography': false,
                'display_gallery': true,
                'get_raw_json': true,
                'callback': function (data) {
                    data = $.parseJSON(data);
                    let post = data.images;

                    $.each(post, function (index, item) {
                        if (index < 8) {
                            $('#instafeed').append('<a href="https://www.instagram.com/p/' + item.node.shortcode + '" target="_blank"><img src="' + item.node.thumbnail_src + '" /></a>');
                        }
                    });

                    $('#instafeed').slick({
                        infinite: true,
                        slidesToShow: 4,
                        autoplay: true,
                        autoplaySpeed: 5000,
                        dots: true,
                        responsive: [{
                                breakpoint: 1024,
                                settings: {
                                    slidesToShow: 3,
                                    slidesToScroll: 3,
                                    infinite: true,
                                    dots: true
                                }
                            },
                            {
                                breakpoint: 600,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1
                                }
                            }
                        ]
                    });
                },
                'styling': false,
                'items': 4,
                'items_per_row': 4,
                'margin': 0
            });

            // let userFeed = new Instafeed({
            //     get: 'user',
            //     userId: '7881000659',
            //     accessToken: '7881000659.1677ed0.40939058d41c4f23ae1816e65ee7624d',
            //     resolution: 'standard_resolution',
            //     template: '<a href="{{link}}" target="_blank" id="{{id}}"><img src="{{image}}" /> <div class="infos"><span class="likes"> {{likes}} </span> <span class="comentario"> {{comments}} </span></div></a>',
            //     sortBy: 'most-recent',
            //     limit: 12,
            //     links: false,
            //     after: function () {
            //         $('#instafeed').slick({
            //             infinite: true,
            //             slidesToShow: 4,
            //             autoplay: true,
            //             autoplaySpeed: 5000,
            //             dots: true,
            //             responsive: [{
            //                     breakpoint: 1024,
            //                     settings: {
            //                         slidesToShow: 3,
            //                         slidesToScroll: 3,
            //                         infinite: true,
            //                         dots: true
            //                     }
            //                 },
            //                 {
            //                     breakpoint: 600,
            //                     settings: {
            //                         slidesToShow: 1,
            //                         slidesToScroll: 1
            //                     }
            //                 }
            //             ]
            //         })
            //     },
            // })

            // userFeed.run()
        },

        thumbProd: function () {
            setTimeout(function () {
                $('.car-gallery .slick-slide').first().find('a').click()
            });

            if (screen < 999) {
                $('.blocoQtd').removeClass('column');

                $('#pagamento-calculado').removeClass('hideme');
            }
        },

        videoProd: function () {
            let video = $('.detalhes-prod #descricao .video').html()
            if (video == undefined) {
                $('.video-prod').hide()
            } else {
                $('.video-prod .video').append(video)
            }
        },

        compreJunto: function () {
            $('.toggle.button.compre.junto').trigger('click')
        },

        qvvt: function () {
            $('.car-relacionados').slick({
                infinite: true,
                slidesToShow: 4,
                slidesToScroll: 1,
                dots: true,
                responsive: [{
                        breakpoint: 1250,
                        settings: {
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 999,
                        settings: {
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 1
                        }
                    }
                ]
            })
        },

        nossasLojas: function () {
            $('#cep').keypress(function (e) {
                if (e.which == 13) {
                    $('#buscar-lojas').trigger('click')
                }
            });

            $('#cep').bind('input', function () {
                $('.go_to_maps').attr('href', 'https://probel.com.br/institucional/439/3193?slwaddress=' + $(this).val());
                $('#storelocator-search_address').val($(this).val());
            });

            // $('#buscar-lojas').click(function(){
            //     let cep = $('#cep').val()

            //     $.ajax({
            //         url: 'https://viacep.com.br/ws/'+cep+'/json/',
            //         dataType: 'jsonp',
            //         crossDomain: true,
            //         contentType: "application/json",
            //         statusCode: {
            //             200: function(data) { 

            //                 $('.lista-lojas .loja, .lista-lojas .loja-nao-encontrada, .cep-invalido').remove()
            //                 if(data.localidade !== undefined){
            //                     console.log(data)
            //                     console.log(data.localidade)
            //                     resgataLojas (data.localidade)
            //                 } else {
            //                     $('.lista-lojas .loja, .lista-lojas .loja-nao-encontrada, .cep-invalido').remove()
            //                     $('#buscar-lojas').after('<p class="cep-invalido">Cep invÃ¡lido</p>') 
            //                 }
            //             } // Ok
            //             ,400: function(msg) { console.log(msg);  } // Bad Request
            //             ,404: function(msg) { 
            //                 $('.lista-lojas .loja, .lista-lojas .loja-nao-encontrada, .cep-invalido').remove()
            //                 $('#buscar-lojas').after('<p class="cep-invalido">Cep invÃ¡lido</p>') 
            //             } // Not Found
            //         }
            //       })
            // })

            // function resgataLojas (localidade) {
            //     $.ajax({
            //         type: "get",
            //         url: "/assets/js/lojas-probel.json",
            //         'Access-Control-Allow-Origin': '*',
            //         success: function (data) {
            //             $.each(data, function(i) {
            //                 if(localidade.toUpperCase() === data[i].MUNICIPIO){
            //                     let linkTel = data[i].TELEFONE;

            //                     $('.lista-lojas').append(
            //                         '<li class="loja">'
            //                             +'<p class="nome-fantasia">' +data[i].NOME_FANTASIA+ '</p>'
            //                             +'<p class="endereco"><b>EndereÃ§o:</b> ' +data[i].ENDEREÃ‡O+ '</p>'
            //                             +'<p class="bairro"><b>Bairro:</b> ' +data[i].BAIRRO+ '</p>'
            //                             +'<p class="municipio"><b>Cidade:</b> ' +data[i].MUNICIPIO+ '</p>'
            //                             +'<p class="uf"><b>Estado:</b> ' +data[i].UF+ '</p>'
            //                             +'<p class="cep"><b>Cep:</b> ' +data[i].CEP+ '</p>'
            //                             +'<p class="telefone"><a href="tel:'+data[i].DDD+linkTel.replace("-", "")+'"><b>Telefone:</b> (' +data[i].DDD+ ') ' +data[i].TELEFONE+ '</a></p>'
            //                             +'<a class="mapBtn" href="https://www.probel.com.br/institucional/439/3193">Ver no mapa</a>'
            //                         +'</li>'
            //                     )
            //                 }
            //             })

            //             let qtdLojas = $('.lista-lojas .loja').length
            //             if(qtdLojas < 1){
            //                 $('.lista-lojas').append('<p class="loja-nao-encontrada"> Ainda nÃ£o temos lojas em sua regiÃ£o =( </p>')
            //             }
            //         },
            //         error: function (request, error) {
            //             console.log(arguments)
            //         }
            //     });
            // }
        },

        menuMobile: function () {

            $('.topo .account-topo_mobile').addClass('hideme');

            if (screen < 999) {

                $('.account-topo').addClass('hideme');

                let account = $('.topo .account-topo_mobile').html()
                $('.topo .btn-menu').click(function () {
                    $('.menu, body').toggleClass('aberto')
                })

                $('.menu .close').click(function () {
                    $('.menu, body').toggleClass('aberto')
                })

                $('.menu ul').prepend('<div class="mobile-account">' + account + '</div>')
                $('.mobile-account .topo .account-topo_mobile').removeClass('hideme');

                $('.mobile-account .account-itens').hide();

                $('.mobile-account h2').click(function (event) {
                    /* Act on the event */

                    $('.mobile-account .account-itens').slideToggle(400);
                });
            }
        },

        homeTipo: function () {
            $('.tipo .selecao ul li').each(function () {
                $(this).click(function () {
                    $(this).find('.btn-ver a').trigger('click')
                })
            })

            if (screen < 999) {
                $('.tipo .selecao ul').slick({
                    infinite: true,
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    dots: true,
                    responsive: [{
                        breakpoint: 500,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                        }
                    }]
                })
            }
        },

        mosaicoMob: function () {
            if (screen < 999) {
                $('.mosaico-mob').slick({
                    infinite: true,
                    slidesToShow: 1,
                    dots: true
                })
            }
        },

        botaoComprarMobile: function () {
            if (screen < 999) {
                $('#preco_fixo').append($('#preco').html());
                $('#max-parcel').append($('.infoPreco #max-p').html().replace("Por: ", ""));
                $('#preco_parcel').append($('.infoPreco #max-value').html());

                $(document).scroll(function () {
                    var y = $(this).scrollTop();
                    if (!$('.btnComprar > .detalhes').hasClass('disable')) {
                        if (y > 850) {
                            $('.compraSegura').hide();
                            $('.precoClass').removeClass('hideme');
                            $('.btnComprarInfo').removeClass('hideme');
                            $('#preco_fixo').removeClass('hideme');
                            $('.btnComprar').addClass('activeBottom');
                        } else {
                            $('.compraSegura').show();
                            $('.precoClass').addClass('hideme');
                            $('.btnComprarInfo').addClass('hideme');
                            $('#preco_fixo').addClass('hideme');
                            $('.btnComprar').removeClass('activeBottom');
                        }
                    }
                });
            }
        },

        slideLojas: function () {
            if (screen < 999) {
                $('.lojas ul').slick({
                    infinite: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: true
                })
            }
        },

        footer: function () {
            if (screen < 999) {
                $('.footer .footer-01-01, .footer .footer-01-02, .footer .footer-01-03').click(function () {
                    $(this).find('.itens').slideToggle()
                });

                $('.footer .footer_principal').addClass('ui four column stackable inverted equal height grid');
            }
        },

        emailHotelaria: function () {
            $('.forulario-contato .enviar').click(function (e) {
                e.preventDefault()
                let nome = $('.forulario-contato input.nome').val()
                let email = $('.forulario-contato input.email').val()
                let empresa = $('.forulario-contato input.empresa').val()
                let telefone = $('.forulario-contato input.telefone').val()
                let cidade = $('.forulario-contato input.cidade').val()
                let estado = $('.forulario-contato input.estado').val()

                $('.forulario-contato .error').remove()
                if (nome == '') {
                    $('.forulario-contato input.nome').after('<p class="error"> Preencha este campo </p>')
                } else if (email == '') {
                    $('.forulario-contato input.email').after('<p class="error"> Preencha este campo </p>')
                } else if (empresa == '') {
                    $('.forulario-contato input.empresa').after('<p class="error"> Preencha este campo </p>')
                } else if (telefone == '') {
                    $('.forulario-contato input.telefone').after('<p class="error"> Preencha este campo </p>')
                } else if (cidade == '') {
                    $('.forulario-contato input.cidade').after('<p class="error"> Preencha este campo </p>')
                } else if (estado == '') {
                    $('.forulario-contato input.estado').after('<p class="error"> Preencha este campo </p>')
                } else {

                    $.ajax({
                        type: "post",
                        url: "https://www.agenciaeplus.io/mensageiro/enviaHotelaria.php",
                        'Access-Control-Allow-Origin': '*',
                        data: {
                            'nome': nome,
                            'email': email,
                            'empresa': empresa,
                            'telefone': telefone,
                            'cidade': cidade,
                            'estado': estado
                        },
                        beforeSend: function () {
                            $(".forulario-contato .enviar").text("Enviando...");
                        },
                        success: function (data) {
                            $('.forulario-contato').html('<h3> Sua mensagem foi enviada e retornaremos em breve! </h3>')
                        },
                        error: function (request, error) {
                            console.log(arguments)
                        }
                    });

                }
            })
        },

        emailcontato: function () {
            $('#submitContact').on('mousedown', function (e) {
                e.preventDefault()

                let nome = $('#formContact input[name="Name"]').val()
                let email = $('#formContact input[name="Email"]').val()
                let telefone = $('#formContact input[name="Phone"]').val()
                let estado = $('#formContact input.search').next('.text').text()
                let cidade = $('#formContact input[name="City"]').val()
                let departamento = $('#formContact select[name="ContactTopic.IdContactTopic"]').val()
                let assunto = $('#formContact input[name="Subject"]').val()
                let mensagem = $('#formContact textarea[name="Message"]').val()

                // console.log(nome, email, telefone, estado, cidade, departamento, assunto, mensagem)

                $('#formContact input, #formContact textarea[name="Message"]').css('border-color', 'rgba(34, 36, 38, 0.15)')
                $('#formContact select[name="ContactTopic.IdContactTopic"]').parent('.selection').css('border-color', 'rgba(34, 36, 38, 0.15)')
                if (nome == '') {
                    $(' input[name="Name"]').css('border-color', '#f00')
                } else if (email == '') {
                    $('#formContact input[name="Email"]').css('border-color', '#f00')
                } else if (telefone == '') {
                    $('#formContact input[name="Phone"]').css('border-color', '#f00')
                } else if (estado == '') {
                    $('#formContact input.search').css('border-color', '#f00')
                } else if (cidade == '') {
                    $('#formContact input[name="City"]').css('border-color', '#f00')
                } else if (departamento == '') {
                    $('#formContact select[name="ContactTopic.IdContactTopic"]').parent('.selection').css('border-color', '#f00')
                } else if (assunto == '') {
                    $('#formContact input[name="Subject"]').css('border-color', '#f00')
                } else if (mensagem == '') {
                    $('#formContact textarea[name="Message"]').css('border-color', '#f00')
                } else {

                    $.ajax({
                        type: "post",
                        url: "https://www.agenciaeplus.io/mensageiro/enviaFaleConosco.php",
                        'Access-Control-Allow-Origin': '*',
                        data: {
                            'nome': nome,
                            'email': email,
                            'telefone': telefone,
                            'estado': estado,
                            'cidade': cidade,
                            'departamento': departamento,
                            'assunto': assunto,
                            'mensagem': mensagem
                        },
                        beforeSend: function () {
                            $('#submitContact').text('Enviando...')
                        },
                        success: function (data) {
                            swal(
                                'Mensagem enviada!',
                                'Obrigado por seu contato! Retornaremos em breve.',
                                'success'
                            )
                            $('#submitContact').text('Enviar')
                        },
                        error: function (request, error) {
                            console.log(arguments)
                        }
                    });
                }

                return
            })
        },

        filtroCategoria: function () {
            if (screen < 999) {
                $('.filtros .ui.accordion .title.active').click()
            }
        },

        alturaCard: function () {
            console.log($('.produto-prateleira .info .dados .freteStatus:contains("Frete")').length);

            if ($('.produto-prateleira .info .dados .freteStatus:contains("Frete")').length) {

                $('#list .produto-prateleira .image').css('height', '150px');
                $('#list .produto-prateleira .info .botoes').css('bottom', '-70px');

                $('#list .produto-prateleira').hover(function () {
                    // over
                    $('.produto-prateleira').css('height', '515px');
                }, function () {
                    // out
                    $('.produto-prateleira').css('height', '460px');
                });

            }
        },

        arrowsProd: function () {
            if ($('.car-gallery.thumbnails .ui.image').length > 1) {
                //REMOVER
                // $('head').append('<style>.slick-slide.slick-current.slick-active {background-color:red;}</style>');
                //REMOVER

                $(document).on('click', '.detalhes.produto .change a', function (event) {
                    event.preventDefault();

                    if ($(this).hasClass('next')) {
                        $('.detalhes.produto .car-gallery .slick-next').trigger('click');
                    } else {
                        $('.detalhes.produto .car-gallery .slick-prev').trigger('click');
                    }

                    setTimeout(function () {
                        $('.detalhes.produto .car-gallery .slick-slide.slick-current a').trigger('click');
                    }, 1000);
                });
            } else {
                $(".change").hide();
            }
        },

        bannnersHome: function () {
            $('.vitrine .banner-center').each(function (index, element) {
                // element == this
                $(this).addClass('centerBanner'+ index);
            });
        },

        detalhesPedidos: function(){
            if($('.dadosPedido').length > 0){
                $('.dadosPedido').parents('.container-center').children('.ui.divider').remove();
                var str = $('#pagePrint > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(6) > div:nth-child(2) > strong:nth-child(1)').text();
                var res = str.replace("ï¿½", " Ú");
                $('#pagePrint > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(6) > div:nth-child(2) > strong:nth-child(1)').text(res);
                var str2 = $('#pagePrint > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(6) > div:nth-child(2) > strong:nth-child(1)').text();
                var res2 = str2.replace("Ãº", "Ú");
                $('#pagePrint > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(6) > div:nth-child(2) > strong:nth-child(1)').text(res2);
            }
        },

        init: function () {
            //Geral
            codeEplusAlpha.accountHeader()
            codeEplusAlpha.campoBusca()
            codeEplusAlpha.slidePrincipal()
            codeEplusAlpha.slideAvisos()
            codeEplusAlpha.slidePrateleira()
            codeEplusAlpha.homeTipo()
            // codeEplusAlpha.alturaCard()
            // codeEplusAlpha.bannersHome()
            codeEplusAlpha.instaFeed('probeloficial')
            codeEplusAlpha.thumbProd()
            codeEplusAlpha.videoProd()
            codeEplusAlpha.compreJunto()
            codeEplusAlpha.qvvt()
            codeEplusAlpha.nossasLojas()
            codeEplusAlpha.arrowsProd()
            codeEplusAlpha.bannnersHome()
            codeEplusAlpha.detalhesPedidos()
            // Mobile
            codeEplusAlpha.menuMobile()
            codeEplusAlpha.mosaicoMob()
            codeEplusAlpha.slideLojas()
            codeEplusAlpha.footer()
            codeEplusAlpha.emailHotelaria()
            codeEplusAlpha.emailcontato()
            codeEplusAlpha.botaoComprarMobile()
        }
    }

    codeEplusAlpha.init()
});