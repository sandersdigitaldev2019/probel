<?php
header('Access-Control-Allow-Origin: *');

session_start();
$senha = 'probel@1940';
$emailRemetente = 'mensageiroprobel@gmail.com';
$nome = $_POST['nome'];
$email = $_POST['email'];
$telefone = $_POST['telefone'];
$cidade = $_POST['cidade'];
$estado = $_POST['estado'];
$departamento = $_POST['departamento'];
$assunto = $_POST['assunto'];
$mensagem = $_POST['mensagem'];

switch ($departamento) {
    case 770: //Duvidas
        // $emailDestinatario = 'rodrigo.freitas@agenciaeplus.com.br';
        $emailDestinatario = 'assistentedecompras@probel.com.br';
        break;
    case 1929: //Rastrear entrega site
        // $emailDestinatario = 'rodrigo.freitas@agenciaeplus.com.br';
        $emailDestinatario = 'entregasite@probel.com.br';
        break;
    case 1930: //Representante Comercial
        // $emailDestinatario = 'rodrigo.freitas@agenciaeplus.com.br';
        $emailDestinatario = 'atendimento@probel.com.br';
        break;
    case 1927: //Hotelaria
        // $emailDestinatario = 'rodrigo.freitas@agenciaeplus.com.br';
        $emailDestinatario = 'hotelaria@probel.com.br';
        break;
    case 1928: //Reclamação
        // $emailDestinatario = 'rodrigo.freitas@agenciaeplus.com.br';
        $emailDestinatario = 'atendimento@probel.com.br';
        break;
    case 1924: //Currículo
        // $emailDestinatario = 'rodrigo.freitas@agenciaeplus.com.br';
        $emailDestinatario = 'atendimento@probel.com.br';
        break;
    case 1436: //Assistência técnica
        // $emailDestinatario = 'rodrigo.freitas@agenciaeplus.com.br';
        $emailDestinatario = 'atendimento@probel.com.br';
        break;
    case 1435: //Trocas e devoluções
        // $emailDestinatario = 'rodrigo.freitas@agenciaeplus.com.br';
        $emailDestinatario = 'atendimento@probel.com.br';
        break;
}



$corpoEmail = " 
    <html>
        <h1> Mensagem recebida do site </h1>
        Nome: {$nome}<br/>
        E-mail: {$email}<br/>
        Telefone: {$telefone}<br/>
        Estado: {$estado}<br/>
        Cidade: {$cidade}<br/>
        Assunto: {$assunto}<br/>
        Mensagem: {$mensagem}<br/>
    </html>
";

require_once 'vendors/phpMailer/PHPMailer.php';
require_once 'vendors/phpMailer/SMTP.php';
require_once 'vendors/phpMailer/Exception.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

$mail = new PHPMailer(true);
try {

     //Server settings
    $mail->SMTPDebug = 1;   
    $mail->CharSet = 'UTF-8';            
    $mail->isSMTP();                               
    $mail->Host = 'smtp.gmail.com';  
    $mail->SMTPAuth = true;             
    $mail->Username = $emailRemetente;
    $mail->Password = $senha;     
    $mail->SMTPSecure = 'TSL';               
    $mail->Port = 587;                               

    //Recipients
    $mail->setFrom($emailRemetente, 'Novo contato pelo site');
    $mail->addAddress($emailDestinatario, '');  
    // $mail->addAddress('rodrigo.freitas@agenciaeplus.com.br', ''); 

    $mail->Subject = "Novo contato pelo site";
    $mail->msgHTML($corpoEmail);
    $mail->AltBody = "de: {$nome}\nemail:{$email}";
        if($comprovante['tmp_name']){
            $mail->addAttachment($comprovante['tmp_name'],$comprovante['name']);
        }

        if ($mail->send()) {
            echo'Email enviado';
            die();
        }
} catch (Exception $e) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
}
die();