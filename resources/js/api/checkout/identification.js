﻿import {
    _alert,
    _confirm
} from '../../functions/message';
import {
    validarEmail
} from "../../functions/validate";
import {
    generateRecaptcha
} from "../../ui/modules/recaptcha";

$(document).ready(function () {
    //checkEmail();
    //ConfirmaEmail();

    checkLogin();
});

function checkLogin() {

    $("#checkLogin").on("click", function () {
        $(this).addClass("loading");
        var strLogin = $("#login").val();
        var googleResponse = $("[id^=googleResponse]", "body").length > 0 ? $("[id^=googleResponse]", "body").val() : "";

        var isValidEmail = validarEmail(strLogin);
        var isValidCpf = $.fn["jetCheckout"].validateCPF(strLogin);
        var isValidCnpj = $.fn["jetCheckout"].validateCNPJ(strLogin);

        // condição
        if (strLogin === "") {
            _alert("", "Informe um E-mail, CPF ou CNPJ", "warning");
            $("#checkLogin").removeClass("loading");
        } else if (!isValidEmail && !isValidCpf && !isValidCnpj) {
            _alert("", "Informe um E-mail, CPF ou CNPJ válido.", "error");
            $("#checkLogin").removeClass("loading");
        } else {
            $.ajax({
                method: "POST",
                url: "/checkout/CheckLogin",
                data: {
                    email: isValidEmail ? strLogin : "",
                    cpfCnpj: (isValidCpf || isValidCnpj) ? strLogin : "",
                    googleResponse: googleResponse
                },
                success: function (response) {
                    if (response.success) {
                        //CAPTURA PARA CATCHLEADS
                        let testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
                        let acceptNewsletter = $('input[name="accept-news"]').is(':checked');
                        if (testEmail.test($('#login').val()) && acceptNewsletter) {
                            $.ajax({
                                url: 'https://dev.sandersdigital.com.br/catchleads/probel.php',
                                type: 'GET',
                                data: {
                                    email_news: $("#login").val(),
                                    formId: '7'
                                },
                                success: function success(res) {
                                    console.log('Enviado ao Catchleads.');
                                    
                                    window.location.href = response.action;
                                },
                                error: function error(request, _error2) {
                                    console.log('Erro ao enviar para Catchleads.');

                                    window.location.href = response.action;
                                }
                            });
                        } else {
                            window.location.href = response.action;
                        }
                    } else {
                        if (response.msg === "") {
                            if (isValidEmail) {
                                $("#identificationForm #email").val(strLogin);
                                $("#identificationForm #cpfCnpj").val("");
                            } else {
                                $("#identificationForm #email").val("");
                                $("#identificationForm #cpfCnpj").val(strLogin);
                            }

                            $("#identificationForm").attr("action", "/checkout/" + response.action);
                            $("#identificationForm").submit();
                        } else {
                            swal('', response.msg, 'error');
                            $("#checkEmail").removeClass("loading");
                        }
                    }
                },
                complete: function () {
                    if ($("[id^=googleVersion_]").length > 0 && typeof grecaptcha !== "undefined") {
                        if ($("[id^=googleVersion_]").eq(0).val() === "2") {
                            grecaptcha.reset();
                        } else {
                            generateRecaptcha($("[id^=googleModule]").val(), "body");
                        }
                    }
                }
            });
        }
        return false;
    });

    $("#login").unbind().keyup(function (e) {
        if (e.keyCode == 32) return false;

        if (e.which == 13) {
            $("#checkLogin").click()
        }
    });
}